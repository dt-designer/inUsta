Main page - http://test.dt-designer.ru/inusta/index.html<br />
Profile page - http://test.dt-designer.ru/inusta/profile.html

<h3>INSTALL</h3>
<ol>
  <li>Установить NODEJS</li>
  <li>Установить GULP <code>npm install --global gulp-cli</code></li>
  <li>Установить BOWER <code>npm install --global bower</code></li>
  <li>Клонировать этот репозиторий</li>
  <li>Установить модули <code>npm install</code></li>
  <li>Установить FrontEnd зависимости <code>bower install</code></li>
</ol>
<p></p>
<p>Как использовать GULP</p>
<ul>
	<li>Run <code>gulp serve</code> to preview and watch for changes</li>
	<li>Run <code>bower install --save &lt;package&gt;</code> to install frontend dependencies</li>
	<li>Run <code>gulp serve:test</code> to run the tests in the browser</li>
	<li>Run <code>gulp</code> to build your webapp for production</li>
	<li>Run <code>gulp serve:dist</code> to preview the production build</li>
</ul>
