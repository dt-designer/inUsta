'use strict';

    //var $ = require('jquery');
    var zIndex;

    /* Exhibiting block sizes and margins */
    function blockSize() {
      if($(document).find('.profile-character').length) {
        $(document).find('.profile-character .item').each(function() {
          $(this).find('.percentage').css({ lineHeight: $(this).find('.percentage').width() + 'px' });
        });
      }
    }
    /* === / === */

    function selectsStyle(elm, ZIndex) {
        elm.hide();
        var parent = elm.parent(),
            options,
            selected = {
                value: '',
                text: '',
                class: ''
            },
            icon = '',
            inputClass;
        elm.find('option').each(function() {
            if($(this).attr('selected') === 'selected') {
                selected.value = $(this).attr('value');
                selected.text = $(this).text();
                selected.class = ' selected';
            } else {
                selected.class = '';
            }
            if(!options) {
                options = '<li class="item' + selected.class + '" data-val="' + $(this).attr('value') + '">' + $(this).text() + '</li>';
            } else {
                options = options + '<li class="item' + selected.class + '" data-val="' + $(this).attr('value') + '">' + $(this).text() + '</li>';
            }
        });
        if( !selected.text && elm.attr('data-placeholder') ) { selected.text = elm.attr('data-placeholder'); }
        if( elm.attr('data-icon') ) { icon = '<i class="icon ' + elm.attr('data-icon') + '"></i>'; }
        if( elm.hasClass('input-lg') ) { inputClass = ' input-lg'; }
        var newElm = '<div class="styledSelect" style="z-index:' + ZIndex + '">' +
            '<a href="" class="actual' + inputClass + '" data-selected="' + selected.value + '">' + icon + '<span>' + selected.text + '</span></a>' +
            '<ul class="_menu">' + options + '</li>' +
        '</div>';
        parent.append(newElm);
    }


    $(document).ready(function() {

        blockSize();

        // DropDown Menu
        $(document).on('click', '.dropDown', function() {
            $(this).toggleClass('active');
        });


        // Styled select element
        zIndex = 999;
        $(document).find('select.styled').each(function() {
            selectsStyle($(this), zIndex);
            zIndex = zIndex - 1;
        });
        $(document).on('click', '.styledSelect', function() {
            $(this).toggleClass('active');
            return false;
        });
        $(document).on('click', '.styledSelect li', function() {
            $(this).parents('.styledSelect').find('li').removeClass('selected');
            $(this).addClass('selected').parents('.styledSelect').find('.actual').attr('data-selected', $(this).attr('data-val')).find('span').text( $(this).text() );
            $(this).parents('.styledSelect').parent().find('select').val($(this).attr('data-val'));
        });


        // User menu toggle
        $('.userButtons').on('click', '.btn-body', function() {
          if(!$(this).parent('.button').hasClass('active')) {
            $('.userButtons .button').removeClass('active');
            $(this).parent('.button').addClass('active');
          }
          return false;
        });


        // Close All opened DropDown
        $(document).click( function(event){
            if( !$(event.target).closest('.dropDown').length ) {
                $('.dropDown').removeClass('active');
                event.stopPropagation();
            }
            if( !$(event.target).closest('.styledSelect').length ) {
                $('.styledSelect').removeClass('active');
                event.stopPropagation();
            }
          if( !$(event.target).closest('.userButtons > .button > .btn-body').length ) {
            $('.userButtons > .button').removeClass('active');
            event.stopPropagation();
          }
        });


        // DatePicker
        if( $(document).find('input.date-picker').length > 0 ) {
            $('input.date-picker').datepicker({
                'dateFormat': 'd MM, y'
            });
        }

    });


    $(window).load(function() {
        blockSize();
    });
    $(window).on('resize', function() {
        blockSize();
    });

